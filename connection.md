# Connection info

Host header: <**{% $headers.host %}**>

Remote IP: <**{% $remoteAddr.hostname %}**>

True-Client-IP header: <**{% $headers["true-client-ip"] %}**>