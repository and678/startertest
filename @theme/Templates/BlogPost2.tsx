import React from 'react';
import styled from 'styled-components';
import { useLocation } from 'react-router-dom';
import { MarkdownWrapper } from '@theme/Markdown/MarkdownWrapper';
import { AuthorAvatar } from '@theme/Blog/AuthorAvatar';
import { AuthorName } from '@theme/Blog/AuthorName';
import { PostDate } from '@theme/Blog/PostDate';
import { H2 } from '@theme/Typography';
import { Link } from '@portal/Link';

import { blogPosts } from '../../blog.page';

export default function BlogPost({ pageProps, children }) {
  return (
    <Wrapper data-component-name="Templates/BlogPost2">
        <Main>
          <MarkdownWrapper>{children}</MarkdownWrapper>
        </Main>
        <Side>
          <AuthorDataWrapper>
            <PostInfo data={pageProps.frontmatter} />
            <PostDate>{new Date(pageProps.frontmatter.date).toDateString()}</PostDate>
            <span>Time to Read: <strong>{pageProps.frontmatter.timeToRead}</strong></span>
          </AuthorDataWrapper>
          <OtherPostsWrapper>
            <H2>Other Blog Posts</H2>
            <OtherPosts posts={blogPosts} />
          </OtherPostsWrapper>
        </Side>
    </Wrapper>
  )
}

function PostInfo({ data }) {
  return (
    <>
      {data.author.avatar && <AuthorAvatar src={data.author.avatar} />}
      <AuthorName>By {data.author.name}</AuthorName>
    </>
  )
}

function OtherPosts({ posts }) {
  const location = useLocation();

  return (
    <OtherPostsList>
      {posts
        .filter(post => '/' + post.link !== location.pathname)
        .map(post => <li key={post.link}><Link to={post.link}>{post.label}</Link></li>)}
    </OtherPostsList>
  )
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  width: 66vw;
  margin: 60px auto 25px;
  padding-top: 25px;
`;

const Main = styled.div`
  margin-right: 60px;
`;

const Side = styled.div`
  display: flex;
  flex-direction: column;
  font-family: var(--font-family-base);
  min-width: 250px;
  padding-top: calc(var(--h1-line-height) + var(--h1-margin-bottom) + 18px);
`;

const AuthorDataWrapper = styled.div`
  text-align: center;
  margin-bottom: 25px;
`;

const OtherPostsWrapper = styled.div`
  margin-top: 25px;
`

const OtherPostsList = styled.ul`
  li {
    margin-bottom: 10px;
  }
`;
