import React from 'react';
import styled from 'styled-components';
import { useLocation } from 'react-router-dom';
import { MarkdownWrapper } from '@theme/Markdown/MarkdownWrapper';
import { AuthorAvatar } from '@theme/Blog/AuthorAvatar';
import { AuthorName } from '@theme/Blog/AuthorName';
import { PostDate } from '@theme/Blog/PostDate';
import { WideTile } from '@theme/ui/Tiles/WideTile';

import { blogPosts } from '../../blog.page';

export default function BlogPost({ pageProps, children }) {
  const location = useLocation();

  return (
    <>
      <Wrapper data-component-name="Templates/BlogPost">
          <PostInfo data={pageProps.frontmatter} />
          <MarkdownWrapper>{children}</MarkdownWrapper>
      </Wrapper>
      <OtherPostsWrapper>
        {blogPosts
          .filter(post => '/' + post.link !== location.pathname)
          .map(post => <OtherPostLink key={post.link} to={post.link} header={post.label} />)}
      </OtherPostsWrapper>
    </>
  )
}

function PostInfo({ data }) {
  return (
    <PostInfoWrapper>
      {data.author.avatar && <AuthorAvatar src={data.author.avatar} />}
      <div>
        <AuthorName>By {data.author.name}</AuthorName>
        <PostDate>{new Date(data.date).toDateString()}</PostDate>
        <span>Time to Read: <strong>{data.timeToRead}</strong></span>
      </div>
    </PostInfoWrapper>
  )
}

const Wrapper = styled.div`
  width: 50vw;
  margin: 25px auto;
  padding: 25px;
  font-size: 16px;
`;

const PostInfoWrapper = styled.div`
  display: flex;
  flex-direction: row;
  font-family: var(--font-family-base);
  margin-bottom: 40px;
`;

const OtherPostsWrapper = styled.div`
  width: 75vw;
  margin: 25px auto;
  display: flex;
`;

const OtherPostLink = styled(WideTile)`
  margin-right: 40px;
`;
