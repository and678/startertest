import styled from 'styled-components';

export const AuthorName = styled.div`
  font-weight: var(--font-weight-bold);
  font-size: 18px;
  margin-bottom: 15px;
`;
