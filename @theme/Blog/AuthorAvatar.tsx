import styled from 'styled-components'

export const AuthorAvatar = styled.img`
  border: 4px solid white;
  border-radius: 50px;
  width: 80px;
  height: 80px;
  filter: drop-shadow(rgba(0, 0, 0, 0.25) 0px 2px 12px);
  margin-right: 25px;
  margin-bottom: 10px;
`;
