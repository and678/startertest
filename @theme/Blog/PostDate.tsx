import styled from 'styled-components';

export const PostDate = styled.div`
  color: #6c6c6c;
  font-size: 16px;
  margin-bottom: 15px;
`
