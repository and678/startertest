---
title: Adding sidebar to the page
sidebar: training
---

# Page with sidebar by name

One can add sidebar to the page with front-matter settings event if the page is not listed in `sidebars.yaml`.

User can specify front-matter param:

```markdown
sidebar: name of sidebar
```

or

```markdown
sidebar: path/to/sidebar.yaml
```
