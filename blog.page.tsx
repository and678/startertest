import React from 'react';
import styled from 'styled-components';
import { WideTile } from '@theme/ui/Tiles/WideTile';

export const blogPosts = [
  {
    label: 'January 2022 Updates',
    link: 'blog/january-2022/'
  },
  {
    label: 'February 2022 Updages',
    link: 'blog/february-2022/'
  },
  {
    label: 'March 2022 Updates',
    link: 'blog/march-2022/',
  },
  {
    label: 'April 2022 Updates',
    link: 'blog/april-2022/'
  }
];

export default function() {
  return (
    <Wrapper>
      {blogPosts.map(blogPost => (
        <StyledWideTile key={blogPost.link} to={blogPost.link} header={blogPost.label} />
      ))}
    </Wrapper>
  )
}

const Wrapper = styled.div`
  margin: 25px auto;
  padding: 25px;
  width: 60vw;
  font-family: var(--font-family-primary);
  padding-left: 20px;
`;

const StyledWideTile = styled(WideTile)`
  margin-right: 20px;
`;
