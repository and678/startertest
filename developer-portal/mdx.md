import { RedocResponse, JsonSchema, OpenApiCodeSample, OpenApiTryIt } from '@redocly/developer-portal/ui';

import {
  Box,
  Button,
  FlexSection,
  Flex,
  Jumbotron,
  H1,
  H2,
  Link,
  ThinTile,
  WideTile,
  Emphasis,
  SectionHeader,
 } from '@redocly/developer-portal/ui';

import icon3 from '../images/icon3.png';

import codeSample from './code-samples/code.sample.js';

# Using MDX

Use MDX by naming the file extension as `.mdx` and importing the components at the top of the file.

You can import a single extension:
```jsx
import { RedocResponse } from '@redocly/developer-portal/ui';
```
Or multiple extensions at the same time:

```jsx
import {
  Box,
  Button,
  FlexSection,
  Flex,
  Jumbotron,
  H1,
  H2,
  Link,
  ThinTile,
  WideTile,
  Emphasis,
  SectionHeader,
 } from '@redocly/developer-portal/ui';
```

We created a library (`@redocly/developer-portal/ui`) with components you can reference.

Try and create an MDX file which renders one of your API's response objects.

With Samples Panel:
```html
<RedocResponse definitionId="petstore" pointer="#/components/responses/PetResponse" />
```
<RedocResponse definitionId="petstore" pointer="#/components/responses/PetResponse" />

Without Samples Panel:

```html
<RedocResponse definitionId="petstore" pointer="#/components/responses/PetResponse" hideSamples={true} />
```
<RedocResponse definitionId="petstore" pointer="#/components/responses/PetResponse" hideSamples={true} />


JsonSchema:

```jsx
<JsonSchema definitionId="petstore" pointer="#/components/schemas/Pet" hideSamples={true} options={{schemaExpansionLevel: 3}} />
```
<JsonSchema definitionId="petstore" pointer="#/components/schemas/Pet" hideSamples={true} options={{schemaExpansionLevel: 3}} />

OpenAPICodeSample:

```jsx
<OpenApiCodeSample
    definitionId="petstore"
    operationId="addPet"
    defaultExample="bee"
    onlyDefaultExample
    onlyDefaultMimeType
    defaultMimeType="application/xml"
    defaultLanguage="JavaScript"
    properties={{ category: { name: 'Redocly' }, tags: [{"id": 15}] }}
    options={{
      hideRequestPayloadSample: true,
      hideSingleRequestSampleTab: true,
      events: {codeSamplesLanguageSwitch: () => console.log('codeSamplesLanguageSwitch')}
    }}
/>
```
<OpenApiCodeSample
    definitionId="petstore"
    operationId="addPet"
    defaultExample="bee"
    defaultLanguage="JavaScript"
    properties={{ category: { name: 'Redocly' }, tags: [{"id": 15}] }}
    options={{
      hideRequestPayloadSample: true,
      hideSingleRequestSampleTab: true,
      events: {codeSamplesLanguageSwitch: () => console.log('codeSamplesLanguageSwitch')}
    }}
/>

OpenApiTryIt:

```jsx
<OpenApiTryIt
  definitionId="petstore"
  operationId="addPet"
  defaultExample="bee"
  parameters={{
    query: { tags: ['test'] }
  }}
  properties={{ category: { name: 'Redocly' }, tags: [{"id": 15}] }}
  /* optional */
  onResponse={({ request, response }) => {
    console.log('request: ', request);
    console.log('response: ', response);
  }}
  securityDefaults={{
    petstore_auth: {
      client_id: 'TEST_CLIENT_ID',
      client_secret: 'TEST_CLIENT_SECRET',
      token: {
        access_token: 'TEST_ACCESS_TOKEN'
      }
    }
/>
```

<OpenApiTryIt
  definitionId="petstore"
  operationId="addPet"
  defaultExample="bee"
  parameters={{
    query: { tags: ['test'] }
  }}
  properties={{ category: { name: 'Redocly' }, tags: [{"id": 15}] }}
  onResponse={({ request, response }) => {
      console.log('request: ', request);
      console.log('response: ', response);
    }
  }
  securityDefaults={{
    petstore_auth: {
      client_id: 'TEST_CLIENT_ID',
      client_secret: 'TEST_CLIENT_SECRET',
      token: {
        access_token: 'TEST_ACCESS_TOKEN'
      }
    }
  }}
/>


## Other components

What other components would you like to see?  Let us know.  We're adding new components.

## Developing new components

A developer familiar with React can create new components.
We left a sample component at `/components/Counter.tsx`.
Inspect it and its usage at `/developer-portal/custom-component.mdx`.

## Snippets

<embed src="./library/_reusable-snippet-example.md" />

## `Samples import`

<pre><code>{codeSample}</code></pre>
