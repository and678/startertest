# Reusable Markdown Snippets

Example .md file:
```markdown
# Main file

Some text

<embed src="path/to/_file.md" />

More text

```

The `path/to/_file.md` includes:
```markdown
Included text
```

Turns into…
```markdown
# Main file

Some text

Included texts

More text
```

## Notes:
* Do not support nested inclusion (snippets within snippets; one snippet containing embed tags for other snippets)
* snippet names should start with underscore ( _ ). E.g `_file-name.md`
* <embed src="./library/_reusable-snippet-example.md" />
* <embed src="../snippets/_another-snippet-example.md" />
