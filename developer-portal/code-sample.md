---
title: OpenApiCodeSample
---

import {
  RedocResponse,
  JsonSchema,
  OpenApiCodeSample,
  OpenApiTryIt,
  OpenApiExample,
  OpenApiRequestBody,
  OpenApiResponse,
} from '@redocly/developer-portal/ui';

# OpenApiCodeSample

<OpenApiCodeSample
  definitionId="petstore"
  operationId="addPet"
  defaultLanguage="Java"
  options={{
    hideRequestPayloadSample: true,
    events: { codeSamplesLanguageSwitch: () => console.log('codeSamplesLanguageSwitch') },
  }}
/>

### Preset request body properties

<OpenApiCodeSample
  definitionId="petstore"
  operationId="addPet"
  defaultLanguage="Go"
  properties={{ status: 'pending', id: 12, tags: [{ name: 'test_subject' }] }}
/>

### Preset request parameter values

<OpenApiCodeSample
  definitionId="petstore"
  operationId="deletePet"
  defaultLanguage="curl"
  parameters={{
    header: {
      api_key: 'your123api456key789',
    },
  }}
/>

# OpenApiExample

<OpenApiExample definitionId="petstoreTest" pointer="#/components/examples/Order" />

# OpenApiRequestBody with sample

<OpenApiRequestBody definitionId="petstore" pointer="#/components/requestBodies/Pet" />

# OpenApiRequestBody without sample

<OpenApiRequestBody
  definitionId="petstore"
  pointer="#/components/requestBodies/Pet"
  hideSamples={true}
/>

# OpenApiResponse with sample

<OpenApiResponse definitionId="petstoreTest" pointer="#/components/responses/UserResponse" />

# OpenApiResponse without sample

<OpenApiResponse
  definitionId="petstoreTest"
  pointer="#/components/responses/UserResponse"
  hideSamples={true}
/>

# OpenApiTryIt

<OpenApiTryIt definitionId="petstore" operationId="addPet" defaultLanguage="curl" />
