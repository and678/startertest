*src* should include relative path to `.md` file

{% admonition type="info" name="Admonitions are supported too" %}
Hello!
{% /admonition %}

As well as code snippets:

```js JavaScript
console.log('Hello world!')
```
```python Python
print('Hello world')
```

Images and links:

* ![ctrl-c](../ctrl-c.png)
* [Analytics page](../analytics.md)
