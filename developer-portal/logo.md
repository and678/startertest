# Change your logo

## Add your logo file

Add your logo file.
If you want to overwrite the existing logo, it's at `images/logo.png`.

## Adjust your siteConfig

The `siteConfig.yaml` has a definition of the path to the header icon.
```yaml
logo:
    href: https://redocly.com
    image: ./images/logo.svg
    altText: alt text for header icon
```

Also, you can change alt attribute for the header icon.
```yaml
meta:
    logoAltText: alt text for header icon
```

Change URL the logo icon is pointing to.
```yaml
meta:
    logoHref: https://redocly.com
```

## Adjust favicon.png

You can overwrite the `favicon.png` file with yours as well.
