import React from 'react';
import styled from 'styled-components';
import { CardsBlock } from '@theme/Cards/CardsBlock';
import { Card } from '@theme/Cards/Card';
import { H1 } from '@theme/Typography';

import clockIcon from './images/clock.png';

export const frontmatter = {
  seo: {
    title: 'Tsx Page with Frontmatter Links',
  },
  links: [
    {
      group: 'Solutions',
      items: [
        {
          'label': 'Set up your infrastructure for hybrid work',
          'page': './solutions/hybrid-infrastructure.md'
        },
        {
          'label': 'Set up secure collaboration',
          'page': './solutions/secure-collaboration.md'
        },
        {
          'label': 'Deploy threat protection',
          'page': './solutions/threat-protection.md'
        },
      ]
    },
    {
      group: 'Architecture',
      items: [
        {
          'label': 'Infographics for your users',
          'page': './architecture/infographics.md'
        },
        {
          'label': 'Architecture templates',
          'page': './architecture/templates.md'
        },
      ]
    },
    {
      group: 'Resources',
      items: [
        {
          'label': 'Courses',
          'href': 'https://youtube.com'
        },
        {
          'label': 'Bootcamp',
          'href': 'https://basecamp.com'
        },
      ]
    },
  ]
};

export default function({ pageProps }) {
  return (
    <Wrapper>
      <H1>Frontmatter links in tsx page</H1>
      <CardsBlock>
        {pageProps.frontmatter.links.map(card => {
          return (
            <Card key={card.label} links={card} title={card.label} icon={clockIcon} />
          )
        })}
      </CardsBlock>
    </Wrapper>
  )
}

const Wrapper = styled.div`
  padding: 40px;
`;