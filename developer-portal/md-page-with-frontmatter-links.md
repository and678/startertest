---
title: Markdown Page with Frontmatter Links
links:
  solutions:
    image: ./images/clock.png
    items:
      - label: Set up your infrastructure for hybrid work
        page: ./solutions/hybrid-infrastructure.md
      - label: Set up secure collaboration
        page: ./solutions/secure-collaboration.md
      - label: Deploy threat protection
        page: ./solutions/threat-protection.md
  architecture:
    image: ./images/clock.png
    items:
      - label: Infographics for your users
        page: ./architecture/infographics.md
      - label: Architecture templates
        page: ./architecture/templates.md
  resources:
    image: ./images/clock.png
    items:
      - label: Courses
        href: https://youtube.com
      - label: Bootcamp
        href: https://basecamp.com
---

# Frontmatter Links in Markdown page

{% cards-block %}

{% card title="Solutions" links=$frontmatter.links.solutions /%}

{% card title="Architecture" links=$frontmatter.links.architecture /%}

{% card title="Resources" links=$frontmatter.links.resources /%}

{% /cards-block %}