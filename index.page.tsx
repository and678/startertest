import * as React from 'react';
import styled from 'styled-components';
import {
  Box,
  FlexSection,
  Flex,
  Jumbotron,
  ThinTile,
  WideTile
} from '@theme/ui';
import { Button } from '@theme/Button';
import {
  Emphasis,
  H1,
  H2,
  SectionHeader
} from '@theme/Typography';

import icon1 from './images/icon1.png';
import launchFastIcon from './images/launch-fast.svg';
import icon3 from './images/icon3.png';

export const frontmatter = {
  seo: {
    title: 'Demo API Portal',
  },
};

export default function () {
  return (
    <>
      <Jumbotron>
        <H1>Redocly training</H1>
        <H2>A starter developer portal with training exercises</H2>
        <Flex p={20} justifyContent="center">
          <Button variant="outlined" size="large" to="https://app.redoc.ly">
            Redocly
          </Button>
          <Button variant="outlined" size="large" to="developer-portal">
            Get started
          </Button>
          <Button variant="outlined" size="large" to="https://redoc.ly/docs">
            Docs
          </Button>
        </Flex>
      </Jumbotron>
      <Box my={25}>
        <SectionHeader>
          Create your <Emphasis> developer portal </Emphasis> today!
        </SectionHeader>
        <StyledFlexSection justifyContent="space-around" flexWrap="wrap">
          <ThinTile to="developer-portal" icon={icon1} header="Rock">
            Lorem ipsum dolor sit amet, pri putent oportere quaerendum in, ea mea justo invenire aliquando. Usu modo
            suavitate ea. Et sed labitur epicurei adipiscing. Nibh zril labitur an usu.Lorem ipsum dolor sit amet, pri
            putent oportere quaerendum in, ea mea justo invenire aliquando. Usu modo suavitate ea. Et sed labitur
            epicurei adipiscing. Nibh zril labitur an usu.Lorem ipsum dolor sit amet, pri putent oportere quaerendum in,
            ea mea justo invenire aliquando. Usu modo suavitate ea. Et sed labitur epicurei adipiscing.
          </ThinTile>
          <ThinTile to="developer-portal/index.md" icon={launchFastIcon} header="Paper">
            Lorem ipsum dolor sit amet, pri putent oportere quaerendum in, ea mea justo invenire aliquando. Usu modo
            suavitate ea. Et sed labitur epicurei adipiscing. Nibh zril labitur an usu.
          </ThinTile>
          <ThinTile to="contact.mdx" icon={icon3} header="Scissors">
            Lorem ipsum dolor sit amet, pri putent oportere quaerendum in, ea mea justo invenire aliquando. Usu modo
            suavitate ea. Et sed labitur epicurei adipiscing. Nibh zril labitur an usu.
          </ThinTile>
        </StyledFlexSection>
        {/* <Jumbotron parallaxEffect bgImage={icon1}>
    <H1>Tutorials</H1>
    <H2>Get setup faster...</H2>
  </Jumbotron> */}
        <SectionHeader>
          <Emphasis>Need help? </Emphasis>
          Try our training exercises.
        </SectionHeader>
        <StyledFlexSection justifyContent="space-around" flexWrap="wrap">
          <WideTile to="developer-portal" header="Rock">
            Lorem ipsum dolor sit amet, pri putent oportere quaerendum in, ea mea justo invenire aliquando. Usu modo
            suavitate ea. Et sed labitur epicurei adipiscing. Nibh zril labitur an usu.
          </WideTile>
          <WideTile to="developer-portal/index.md" header="Paper">
            Lorem ipsum dolor sit amet, pri putent oportere quaerendum in, ea mea justo invenire aliquando. Usu modo
            suavitate ea. Et sed labitur epicurei adipiscing. Nibh zril labitur an usu.
          </WideTile>
          <WideTile to="developer-portal/index.md" header="Scissors">
            Lorem ipsum dolor sit amet, pri putent oportere quaerendum in, ea mea justo invenire aliquando. Usu modo
            suavitate ea. Et sed labitur epicurei adipiscing. Nibh zril labitur an usu.
          </WideTile>
          <WideTile to="developer-portal/index.md" header="Go">
            Lorem ipsum dolor sit amet, pri putent oportere quaerendum in, ea mea justo invenire aliquando. Usu modo
            suavitate ea. Et sed labitur epicurei adipiscing. Nibh zril labitur an usu.
          </WideTile>
        </StyledFlexSection>
      </Box>
    </>
  );
}

const StyledFlexSection = styled(FlexSection)`
  margin: 0 auto;
`
