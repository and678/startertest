---
title: Overview
---

# Introduction

Build a developer portal.

### Overview

The **Redocly developer portal** enables you to:
* Create, edit, and manage a significant number of APIs.
* Write contextual documentation in markdown (or MDX).
* Integrate interactive API samples right into the contextual descriptions.
* Generate high-quality API reference pages.
* Style and theme it to match your brand guidelines.
* Control the navbar, footer and sidebar structure and contents.
* Diagram concepts using docs-like-code integrations.
* Extend functionality with custom components.
* Restrict access to portal content for specific groups of users.
