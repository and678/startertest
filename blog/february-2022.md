---
template: '@theme/Templates/BlogPost'
author:
  name: Swapnil Ogale
  avatar: https://redocly.com/static/swapnil-34297663205bdd0ac175bb3031095997.png
date: 04-27-2022
timeToRead: 5 min
---


![February 2022](https://redocly.com/static/blog-6-cc4c6d0fa664694df9f1fc0c0b7df266.png)

February hasn't been the most of inspiring of months at Redocly. We have some really wonderful teammates currently in Ukraine who are right now going through a range of emotions.

Our CEO, Adam Altman, penned this Ukraine war update with some news, and what you can do to help.

In spite of all this, our developers have been shipping new features and working on our products to make them better for our customers.

Let's highlight some of the new features, enhancements and documentation-related news from February 2022.


We've updated our pricing page with changes to usages across some of our plans:

- You can now have 2 users on the Starter plan (previously 1).
- You can now have 3 users on the Basic plan (previously 2).
- Changes to the API registry validations and bundles per month across all our plans.
- Clarified doc builds per month usage across all our plans.

If you need more information, feel free to reach out to us using the Contact us page.

## Workflows

**Pagination on the People tab**

We added the pagination feature to the People tab, which is helpful when you have a lot people in your organization.

**Download Apigee X and Apigee proxy bundles**

Added Download links on the Organization settings > API gateways page that allow enterprise customers to download Apigee X and Apigee Edge proxy bundles.

You can use the proxy bundle to set up your Apigee integration. To find out more, refer to our Set up the Apigee proxy topic.