---
author:
  name: Adam Altman
  avatar: https://redocly.com/static/adam-b2e77696e09f69bff10f9bc393f45249.png
date: 05-03-2022
timeToRead: 10 min
---

## Redocly rebrands as Remockly

The Redocly brand is synonymous with quality API documentation. And we're so much more than that.

"Redocly does much more than API docs. We searched the world far and wide for marketing experts and determined the fastest way to communicate the breadth of our offering is to rename the company," said co-founder and CTO Roman Hotsiy.

![Remockly](https://redocly.com/static/678e41650a1d4659dcb997ab050f143e/9be90/remockly-logo.png)

Renaming the company wasn't too difficult.

## We knew we had a problem when...

"We have happy customers who like our product. I was doing a Zoom welcome call, and Rich and Matt asked if they could have a Redocly t-shirt. A few weeks later, and I received a photo and message in my inbox," said co-founder and CEO Adam Altman.

Rich said, "When people ask what that logo is I’ll tell them about this awesome API documentation company!"

This was when we knew we had a problem. We're more than an awesome API documentation company.

## More than API documentation

Besides API documentation, we do:

- API authoring
- API governance
- API registry
- Guides, tutorials, and other contextual content
- Access controls
- Developer portals
- Mock servers
- Code generation

"We're expanding into more areas. Our newest products include an API security product that detects the OWASP top ten list of API security risks, world's first hypermedia NFT to guard against breaking changes, and AI-generated API design products which will pave the path towards web4," said Roman Hotsiy.*