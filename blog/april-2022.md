---
template: '@theme/Templates/BlogPost2'
author:
  name: Swapnil Ogale
  avatar: https://redocly.com/static/swapnil-34297663205bdd0ac175bb3031095997.png
date: 04-27-2022
timeToRead: 5 min
---

# April 2022 Updates

![March 2022 Updates Banner](https://redocly.com/static/blog-8-1d4b2ddfd9a44368b22fcdf1d6d9c51f.jpg)

March was a super productive month at Redocly. Our developer teams delivered many cool features across the board and I am excited to share all of these with you.

Looking back 12 months, we had a very similar March in 2021, so maybe March has a touch of something special going for it.

Let's run through some of the new features, enhancements and documentation-related news from March 2022.

Firstly, we switched over our website and the Redocly app officially to the redocly.com domain! Easy to remember and straight-forward to work with.

## Workflows

Mock server feature

We added the mock server feature to our registry that lets you test your APIs while you design and develop them.

Add your API definitions to the Redocly API registry and enable the mock server for all branches in the API project settings. The API version Overview page displays the mock server link that you can copy and use in any API testing tool to send requests.

For more information on how to use this feature, refer to the mock server quickstart guide.

{% admonition type="info" name="Attention" %}
The mock server is available only to customers on the Pro and Enterprise plans.
{% /admonition %}

**Simplified configuration file**

The Redocly configuration file is used by multiple Redocly apps (OpenAPI CLI, Workflows, Reference and VS Code extension) to help you control their behavior.

In the latest releases of these apps, we implemented a new format of the configuration file. Our goal was to simplify its usage, improve the integration of Redocly apps, and standardize the way they interpret the configuration file.

To understand what has changed in the configuration file and how to start using the new options, read our migration guide.